FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test30.sh"]

COPY test30.sh /usr/bin/test30.sh
COPY target/test30.jar /usr/share/test30/test30.jar
